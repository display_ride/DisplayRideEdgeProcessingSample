package com.displayride.edgeprocessingsample;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Set;

import static com.displayride.edgeprocessingsample.DisplayRideInterfaceService.SAMPLE_ACK;
import static com.displayride.edgeprocessingsample.DisplayRideInterfaceService.COMMAND;
import static com.displayride.edgeprocessingsample.DisplayRideInterfaceService.DISPLAYRIDE_START_RECORDING;
import static com.displayride.edgeprocessingsample.DisplayRideInterfaceService.DISPLAYRIDE_STOP_RECORDING;
import static com.displayride.edgeprocessingsample.DisplayRideInterfaceService.IS_RECORDING;
import static com.displayride.edgeprocessingsample.DisplayRideInterfaceService.PAYLOAD;
import static com.displayride.edgeprocessingsample.DisplayRideInterfaceService.SENT_MESSAGES;

public class DisplayRideInterfaceReceiver extends BroadcastReceiver {
    static final String TAG = "DR_InferfaceReceiver";
    static final String ACK = "ack";

    // ~150ms response time
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action == null || !action.equals(SAMPLE_ACK)) {
            Log.w(TAG, "invalid action");
            return;
        }
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        Set<String> set = sp.getStringSet(SENT_MESSAGES, null);
        if (set == null || set.size() <= 0) {
            Log.w(TAG, "no messages to match");
            return;
        }

        // match message with acknowledgement
        String payload = intent.getStringExtra(PAYLOAD);
        for (String message : set) {
            if (!message.equals(payload))
                continue;
            set.remove(message);
            sp.edit().putStringSet(SENT_MESSAGES, set).apply();

            String command;
            try {
                JSONObject jsonObject = new JSONObject(message);
                command = jsonObject.getString(COMMAND);
            } catch (JSONException e) {
                Log.e(TAG, e.getMessage());
                return;
            }

            if (intent.getBooleanExtra(ACK, false)) {
                Log.d(TAG, "received: " + message);
                switch (command) {
                    case DISPLAYRIDE_START_RECORDING:
                        sp.edit().putBoolean(IS_RECORDING, true).apply();
                        break;
                    case DISPLAYRIDE_STOP_RECORDING:
                        sp.edit().putBoolean(IS_RECORDING, false).apply();
                        break;
                    default:
                }
            } else {
                Log.d(TAG, "no-op received: " + message);
                switch (command) {
                    case DISPLAYRIDE_START_RECORDING:
                    case DISPLAYRIDE_STOP_RECORDING:
                        sp.edit().putBoolean(IS_RECORDING, !sp.getBoolean(IS_RECORDING, false)).apply();
                        break;
                    default:
                }
            }
            return;
        }
        Log.w(TAG, "no matching message");
    }
}
