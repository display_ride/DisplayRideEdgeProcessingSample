package com.displayride.edgeprocessingsample;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.preference.PreferenceManager;

public class StartupReceiver extends BroadcastReceiver {
    static final String TAG = "DR_SampleStartReceiver";

    // action broadcasted with Standalone demo mode
    static final String SAMPLE_DEMO_STARTUP = "sample.intent.action.DEMO_STARTUP";

    //couple min from start to receive any broadcast
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent == null) return;
        String action = intent.getAction();
        if (action == null) return;
        if (!action.equals(SAMPLE_DEMO_STARTUP)) return;
//        if (!action.equals(Intent.ACTION_BOOT_COMPLETED)) return;

        PreferenceManager.getDefaultSharedPreferences(context).edit().clear().commit();

        Intent i = new Intent(intent);
        i.setClass(context, DisplayRideInterfaceService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            context.startForegroundService(i);
        else
            context.startService(i);
    }
}
