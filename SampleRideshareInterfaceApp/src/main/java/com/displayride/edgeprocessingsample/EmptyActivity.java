package com.displayride.edgeprocessingsample;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

public class EmptyActivity extends Activity {
    static final String TAG = "DR_SampleActivity";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.v(TAG, "empty activity");
        finish();
    }
}
