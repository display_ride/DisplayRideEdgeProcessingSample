package com.displayride.edgeprocessingsample;

import android.app.NotificationChannel;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

// mock polling service
public class DisplayRideInterfaceService extends Service {
    static final String TAG = "DR_SampleService";
    final String ACCESSORY_ID = createRandomAccessoryID(); //This is used to create the Trip ID

    @Override
    public void onCreate() {
        super.onCreate();
        NotificationCompat.Builder builder;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            builder = new NotificationCompat.Builder(getApplicationContext(), NotificationChannel.DEFAULT_CHANNEL_ID);
        } else {
            builder = new NotificationCompat.Builder(getApplicationContext()).setPriority(NotificationCompat.PRIORITY_DEFAULT);
        }
        builder.setSmallIcon(R.mipmap.ic_launcher)
                .setOngoing(true)
                .setTicker("sample service")
                .setContentTitle("sample")
                .setContentText("text");
        //we need this service to stay in forground inorder to prevent the service from being killed by android
        startForeground(1, builder.build());
    }

    @Override
    public void onDestroy() {
        Log.v(TAG, "onDestroy");
        if (t != null)
            t.interrupt();
    }

    static final long MOCK_RECORD_DURATION = 15 * 60 * 1000;
    static final long MOCK_WAIT_DURATION = 5 * 60 * 1000;
    Thread t;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent.getBooleanExtra("demo_toggle", false)) {
            Log.v(TAG, "start demo");

            t = new Thread() {
                @Override
                public void run() {
                    try {
                        while (!isInterrupted()) {
                            sendRecordingCommand(isRecording());
                            sleep(MOCK_RECORD_DURATION);
                            sendRecordingCommand(isRecording());
                            sleep(MOCK_WAIT_DURATION);
                        }
                    } catch (InterruptedException e) {
                        Log.v(TAG, "thread interrupted");
                    }
                }
            };
            t.start();
        } else {
            Log.v(TAG, "stop demo");
            if (t != null)
                t.interrupt();
        }
        return START_STICKY;
    }

    void sendRecordingCommand(boolean is_recording) {
        if (is_recording)
            stopRecording();
        else
            startRecording();
        Log.d(TAG, "sent " + (is_recording ? "stop" : "start"));
    }

    // necessary constants
    static final String DISPLAYRIDE_ACTION_COMMAND = "displayride.intent.action.COMMAND";
    static final String DISPLAYRIDE_START_RECORDING = "displayride.COMMAND.START_RECORDING";
    static final String DISPLAYRIDE_STOP_RECORDING = "displayride.COMMAND.STOP_RECORDING";
    static final String COMMAND = "command";
    static final String PAYLOAD = "payload";
    static final String TRIPID = "trip_id";
    static final String RESPONSE_ACTION = "response_action";

    // other constants
    static final String SENT_MESSAGES = "sent_messages";
    static final String IS_RECORDING = "is_recording";
    static final String SAMPLE_ACK = "sample.intent.action.ACK";

    void sendDisplayRideCommand(JSONObject jsonObject) {
        String payload = jsonObject.toString();
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Set<String> set = sp.getStringSet(SENT_MESSAGES, null);
        if (set == null) set = new TreeSet<>();
        set.add(payload);
        sp.edit().putStringSet(SENT_MESSAGES, set).apply();
        Intent i = new Intent(DISPLAYRIDE_ACTION_COMMAND);
        i.putExtra(PAYLOAD, payload);
        sendBroadcast(i);
    }

    void startRecording() {
        Map<String, String> extras = new TreeMap<>();
        extras.put("randint", String.valueOf((int)(Math.random() * 1000)));
        extras.put(TRIPID, createTripId()); //we create a set with all the paramaters we need to pass to the device.
        sendDisplayRideCommand(startRecordingCommand(extras));
    }

    void stopRecording() {
        sendDisplayRideCommand(stopRecordingCommand());
    }

    JSONObject startRecordingCommand(Map<String, String> uploadExtras) {
        return createCommand(DISPLAYRIDE_START_RECORDING, uploadExtras);
    }

    JSONObject stopRecordingCommand() {
        return createCommand(DISPLAYRIDE_STOP_RECORDING, null);
    }

    JSONObject createCommand(String command, Map<String, String> extras) {
        try {
            JSONObject jsonObject = new JSONObject().put(COMMAND, command);
            if (extras != null) {
                for (Map.Entry<String, String> entry : extras.entrySet()) {
                    jsonObject.put(entry.getKey(), entry.getValue());
                }
            }
            jsonObject.put(RESPONSE_ACTION, SAMPLE_ACK);
            return jsonObject;
        } catch (JSONException e) {
        }
        return null;
    }

    boolean isRecording() {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        return sp.getBoolean(IS_RECORDING, false);
    }

    private String createRandomAccessoryID(){
        double value = (Math.random() * 9000000000L) + 1000000000;
        long roundedValue = Math.round(value);
        return Long.toString(roundedValue);
    }

    String createTripId(){
        return ACCESSORY_ID + Long.toString(System.currentTimeMillis());
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
